---
title: "The Solitude Book"
subtitle: "The primary reference for the Solitude programming language."
author: "Marijn van Wezel"
titlepage: true
toc-own-page: true
header-right: "First draft"
book: true
date: "October 13th, 2020"
keywords: [Specification, Reference, Solitude]
---
