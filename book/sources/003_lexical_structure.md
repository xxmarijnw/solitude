# Lexical structure

## Input format
Solitude input is interpreted as a sequence of Unicode characters encoded as UTF-8. This book will use the unqualified term "character" to refer to a Unicode code point in a source text.

Every code point is entirely distinct. For isntance, upper and lower case letters are treated as different characters.

### Character classes
The following terms are used to denote specific Unicode character classes:

```
newline      /* a newline character, Unicode code point U+000A */
character    /* any arbitrary Unicode character */
letter       /* any arbitrary Unicode character that is classified as a "Letter" */
digit        /* any arbitrary Unicode character that is classified as a "Digit" */
```

## Keywords
Solitude has two types of keywords:

* Strict keywords: These keywords have a defined meaning and are usable in the current definition of the language;
* Reserved keywords: These keywords do not have a defined meaning and are not usable in the current definition of the language.

Keywords are character sequences that are reserved by the language, because the word has special meaning. These keywords are part of the syntax and can not be used as an identifier. 

For example, the word `struct` is reserved in the `C` programming language. Therefore, `int count` is valid, but `int struct` is not. 

### Strict keywords
These keywords can only be used in their correct context. They can not be used as the name of:

* Variables
* Function parameters
* Function arguments
* Functions
* Types
* Constants

> currently empty

### Reserved keywords
These keywords have the same restrictions as `strict keywords`, but are currently not in use.

```plaintext
function
type
for
do
while
in
as
if
elseif
else
return
switch
case
break
continue
use
mutable
constant
enum
integer
float
double
string
char
void
array
```

## Identifiers
> **Grammar:**
>
> alpha = {'a'..'z' | 'A'..'Z'}
>
> identifier = { alpha ~ (alpha | "\_")+ }

An identifier is any non-empty string of the following form:

* The first character is either an upper or lowercase letter (`a-zA-Z`);
* The remain characters are alphanumeric or `_`;
* The identifier is not a keyword.

## Comments
> **Grammar:**
>
> comment = {"#" ~ (!NEWLINE ~ ANY)+}

A comment is a pound symbol (also known as a __hashtag__) followed by any number of Unicode symbols. A comment ends when a newline character (`\n` | `\r\n` | `\r` ) is reached.

### Examples
```
# A comment can be placed in anywhere in the document
# Multiline comments are not supported, and each line is required
# to be prefixed by a pound symbol.
```

## Whitespace
> **Grammar:**
>
> whitespace = {PATTERN_WHITE_SPACE+}

Whitespace is any non-empty string containing only characters that have the [Pattern_White_Space](https://www.unicode.org/reports/tr31/) Unicode property. This property has the following characters:

* `U+0009` (horizontal tab, `'\t'`)
* `U+000A` (line feed, `'\n'`)
* `U+000B` (vertical tab)
* `U+000C` (form feed)
* `U+000D` (carriage return, `'\r'`)
* `U+0020` (space, `' '`)
* `U+0085` (next line)
* `U+200E` (left-to-right mark)
* `U+200F` (right-to-left mark)
* `U+2028` (line separator)
* `U+2029` (paragraph separator)

Solitude is not a free-form language. Therefore, whitespace is semantically significant. Solitude adheres to the _off-side rule_. This means that code blocks are expressed by their indentation and statements are separated by newlines. This not only enfoces well formatted source code, it also makes it easier to read and type.

The off-side rule can be implemented during the lexical analysis phase. Increasing the indentation results in the lexer outputting an `INDENT` token, and decreasing the indentation results in the lexer outputting a special `DEDENT` token. The `DEDENT` token is not a real character, since it exists nowhere in the source code.

The lexer also has to keep an index of the current indentation level. Therefore, the grammar of Solitude is not _context-free_.

The grammar accepts only accepts eight spaces as a valid `INDENT` token. This was chosen because it is the default for most text editors and because it makes the grammar and parser easier to implement.

## Tokens
Tokens are primitive productions in the grammar. A primitive has no parser ambiguities that need to be resolved. A primitive terminates the grammar.

Solitude has the following kinds of tokens:

* Keywords
* Identifiers
* Literals
* Punctuation
* Delimiters


