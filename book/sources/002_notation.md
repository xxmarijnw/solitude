# Notation

All _Lexer_ and _Syntax_ grammar snippets are in the [pest](https://github.com/pest-parser/pest) parsing expression grammar.
