# Introduction
This book is the primary reference for the Solitude programming language. It consists of three different kinds of material:

* Chapters that informally describe a topic and its usage;
* Chapters that informally describe runtime services, debugging tools and so forth;
* An appendix that formally describes the syntax and semantics of the Solitude programming language.

This book attemps to be exact and complete. However, this book mostly uses the English language in favor of formal specifications. This makes the book more understandble and more fun to read, but it leaves slight room for ambiguity.

This book does not specify any implementation details, since such an implementation does not exist at the time of writing this.

## Typesetting conventions
This book has certain conventions in how it displays information. These conventions are documented here:

* Statements that define new a new term are in _italics_. 

* Technical terms, such as function names, are typesetted as `inline code blocks`.

* Notes are in blockquotes and start with the word "Note:" in **bold**.

* Snippets of source code are inside code blocks. Since no highlighter exists for Solitude, the code will not be highlighted.

## Contributing
Be bold and contribute to this book! You can contribute by opening an issue or by sending a pull request to [the Solitude repository](https://gitlab.com/xxmarijnw/solitude).

## Compiling the book
This book can be compiled using the supplied Makefile. It uses Pandoc to convert the Markdown files to \LaTeX, which gets compiled to a PDF.

Any Markdown files in the `sources` directory is automatically linked during the compilation. The files are linked in alphabetical order.
